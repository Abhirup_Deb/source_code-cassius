import pyttsx3
import datetime
import speech_recognition as sr
import wikipedia
import webbrowser
import os
import sys
import urllib
import random
import smtplib
import Crypt
from PyDictionary import PyDictionary
from googlesearch import search
from nltk.corpus import wordnet
import ety

engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')
# print(voices[1].id) --> To see the number of voices available
engine.setProperty('voice', voices[0].id)


def speak(audio):
    engine.say(audio)
    engine.runAndWait()


def wishMe():
    # wishes the user on time of the day
    hour = int(datetime.datetime.now().hour)
    if hour >= 0 and hour < 12:
        speak("Good Morning Deb!")
    elif hour >= 12 and hour <= 18:
        speak("Good Afternoon Deb!")
    else:
        speak("Good Evening!")
    speak("I am your Assistant Sir, How may I help you!")


def takeCommand():
    # it takes microphone input from the user and returns the string output

    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening......")
        r.pause_threshold = 1
        audio = r.listen(source)

    try:
        print("Recognizing....")
        query = r.recognize_google(audio, language='en-in')
        print(f"User Said: {query}\n")

    except Exception as e:
        print(e)
        print("Say that Again Please!!")
        return "None"
    return query


def searchOnGoogle(query, ouputList):
    speak("The Top 5 Search Results from Google are listed below:")
    for output in search(query):
        print(output)
        outputList.append(output)
    return outputList


def openLink(outputList):
    speak("Here's the First link For you Sir...")
    webbrowser.open(outputList[0])


def sendEmail(to, content):
    Crypt.Login(to, content)


def gotoYoutube(query_string):
    query_string = urllib.parse.urlencode({"search_query": query})
    search_string = str("http://www.youtube.com/results?" + query_string)
    speak("Here's what you asked for Sir!")
    webbrowser.open_new_tab(search_string)


def fetchDictionary(word):
    dictionary = PyDictionary()
    mean = {}
    mean = dictionary.meaning(word)
    synonyms = []
    antonyms = []

    speak("Alright. Here is the information you asked for.")

    for key in mean.keys():
        speak("When "+str(word)+" is used as a "+str(key) +
              " then it has the following meanings")
        for val in mean[key]:
            print(val)
        print()

    speak("The possible synonyms and antonyms of "+str(word)+" are given below.")
    for syn in wordnet.synsets(word):
        for l in syn.lemmas():
            if l.name() not in synonyms:
                synonyms.append(l.name())
            if l.antonyms() and l.antonyms()[0].name() not in antonyms:
                antonyms.append(l.antonyms()[0].name())

    print("Synonyms: ", end=" ")
    print(' '.join(synonyms), end=" ")
    print("\n")
    print("Antonyms: ", end=" ")
    print(' '.join(antonyms), end=" ")
    print("\n")

    ori = ety.origins(word)
    if len(ori) > 0:
        speak("There are "+str(len(ori))+" possible origins found.")
        for origin in ori:
            print(origin)
    else:
        speak("I'm sorry. No data regarding the origin of "+str(word)+" was found.")


# Driver__Code
if __name__ == "__main__":
    wishMe()
    while True:
        query = takeCommand().lower()

        if 'wikipedia' in query:
            speak('Searching Wikipedia....')
            query.replace("wikipedia", "")
            results = wikipedia.summary(query, sentences=2)
            speak("According to Wikipedia \n" + results)
            print(results)

        elif 'open youtube' in query:
            webbrowser.open("youtube.com")

        elif 'open google' in query:
            webbrowser.open("google.com")

        elif 'open stackoverflow' in query:
            webbrowser.open("stackoverflow.com")

        elif 'play music' in query:
            music_dir = 'E:\\Music'
            songs = os.listdir(music_dir)
            i = random.randint(1, len(songs)-1)
            print(songs)
            os.startfile(os.path.join(music_dir, songs[i]))

        elif 'time' in query:
            strTime = datetime.datetime.now().strftime("%H:%M:%S")
            speak(f"Sir, the time is {strTime}")

        elif 'open code' in query:
            codePath = "C:\\Users\\USER\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe"
            os.startfile(codePath)

        elif 'email' in query:
            try:
                speak("what do I say?")
                content = takeCommand()
                # to =  # Hardcode the To field to the receiver's e-mail
                sendEmail(to, content)
                speak("The Email has been sent")
            except Exception as e:
                print(e)
                speak("Sorry sir, I am not able to do so at the moment")

        elif 'search' in query:
            outputList = []
            speak("What should I search For?")
            query = takeCommand()
            searchOnGoogle(query, outputList)
            speak('Should I open up the first link sir?')
            query = takeCommand()
            if 'yes' in query or 'sure' in query or 'ofcourse' in query:
                openLink(outputList)
            if 'no' in query:
                speak('Alright Sir!')

        elif 'play on youtube' in query:
            speak('What should I look up for ?')
            query = takeCommand()
            gotoYoutube(query)

        elif 'open dictionary' in query or 'dictionary' in query:
            speak('What do you wish to look up??')
            word = takeCommand()
            fetchDictionary(word)

        elif 'that will be all' in query or 'bye' in query or 'close' in query:
            speak('Have a Nice Day sir!')
            sys.exit()
