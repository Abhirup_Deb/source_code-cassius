**Cassius:: The Desktop Assistant**  
*This is Cassius, your very own Desktop Assistant. It perfoms a plethora of tasks that makes your day to day life easier. For that purpose, the following steps are necessary::*    
**Step-1 -->**  
Install all the necessary requirements, by looking at the imports section of the Script.py file. Note that, the Crypt file is included in this repo itself and is tailored for this application only.  
**Step-2 -->**  
For the Email functionality, put your Email-Id and Password as **Comma-Separated-Values** in the **access.csv** file. Now, to encrypt that file for security so that no one can read your email by chance, if the application is on, open the **Crypt.py** file and run the **Encryptor()** function once, and then comment it out.
If by any chance you re in trouble with the access.csv file, run the **Decryptor()** function a number of times.