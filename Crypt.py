#=============== Key Generator ===============#
import smtplib
import csv
from cryptography.fernet import Fernet
store = []


def KeyGenerator():
    key = Fernet.generate_key()

    with open('filekey.key', 'wb') as filekey:
        filekey.write(key)


# ==========Encryptor===============
def Encryptor():
    with open('filekey.key', 'r') as filekey:
        key = filekey.read()

    fernet = Fernet(key)

    with open('access.csv', 'rb') as file:
        original = file.read()

    encrypted = fernet.encrypt(original)

    with open('access.csv', 'wb') as enc_file:
        enc_file.write(encrypted)


# ----------Decryptor--------------
def Decryptor():
    with open('filekey.key', 'r') as filekey:
        key = filekey.read()

    fernet = Fernet(key)
    with open('access.csv', 'rb') as enc_file:
        encrypted = enc_file.read()

    decrypted = fernet.decrypt(encrypted)
    with open('access.csv', 'wb') as dec_file:
        dec_file.write(decrypted)

    with open('access.csv', 'r') as file:
        original = csv.reader(file)
        for lines in original:
            store = lines

    return store

# ============Login===============


def Login(to, content):
    s = Decryptor()
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    # print(s[0], s[1])
    server.login(s[0], s[1])
    server.sendmail(s[0], to, content)
    server.close()
    Encryptor()


# Decryptor()
